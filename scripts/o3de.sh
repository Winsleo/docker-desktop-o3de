#!/bin/bash
source /opt/ros/*/setup.bash
apt update -y && apt install -y --no-install-recommends git git-lfs libstdc++-12-dev clang ninja-build
apt install -y --no-install-recommends libglu1-mesa-dev libxcb-xinerama0 libxcb-xinput0 libxcb-xinput-dev libxcb-xfixes0-dev libxcb-xkb-dev libxkbcommon-dev libxkbcommon-x11-dev libfontconfig1-dev libpcre2-16-0 zlib1g-dev mesa-common-dev libunwind-dev libzstd-dev
apt install -y --no-install-recommends ros-${ROS_DISTRO}-ackermann-msgs ros-${ROS_DISTRO}-control-toolbox ros-${ROS_DISTRO}-nav-msgs ros-${ROS_DISTRO}-gazebo-msgs ros-${ROS_DISTRO}-xacro
git lfs install
git clone https://github.com/o3de/o3de.git && cd o3de && git checkout 2310.3 && cd ..
git clone https://github.com/o3de/o3de-extras && cd o3de-extras && git checkout 2310.3 && cd ..
export O3DE_HOME=/opt/o3de
export O3DE_EXTRAS_HOME=/opt/o3de-extras
${O3DE_HOME}/python/get_python.sh
${O3DE_HOME}/scripts/o3de.sh register --this-engine
${O3DE_HOME}/scripts/o3de.sh register --gem-path ${O3DE_EXTRAS_HOME}/Gems/ROS2
cd ${O3DE_EXTRAS_HOME}
git lfs install && git lfs pull
${O3DE_HOME}/scripts/o3de.sh register --gem-path ${O3DE_EXTRAS_HOME}/Gems/ProteusRobot
${O3DE_HOME}/scripts/o3de.sh register --gem-path ${O3DE_EXTRAS_HOME}/Gems/RosRobotSample
${O3DE_HOME}/scripts/o3de.sh register --gem-path ${O3DE_EXTRAS_HOME}/Gems/WarehouseAssets
${O3DE_HOME}/scripts/o3de.sh register --gem-path ${O3DE_EXTRAS_HOME}/Gems/WarehouseSample
${O3DE_HOME}/scripts/o3de.sh register --template-path ${O3DE_EXTRAS_HOME}/Templates/Ros2FleetRobotTemplate
${O3DE_HOME}/scripts/o3de.sh register --template-path ${O3DE_EXTRAS_HOME}/Templates/Ros2RoboticManipulationTemplate
${O3DE_HOME}/scripts/o3de.sh register --template-path ${O3DE_EXTRAS_HOME}/Templates/Ros2ProjectTemplate
export PROJECT_NAME=RobotSimProject
export PROJECT_PATH=/opt/o3de/projects/${PROJECT_NAME}
${O3DE_HOME}/scripts/o3de.sh create-project --project-path ${PROJECT_PATH} --template-path ${O3DE_EXTRAS_HOME}/Templates/Ros2ProjectTemplate 
cd ${PROJECT_PATH}
cmake -B build/linux -G "Ninja Multi-Config" -DLY_DISABLE_TEST_MODULES=ON -DCMAKE_EXPORT_COMPILE_COMMANDS=ON -DLY_STRIP_DEBUG_SYMBOLS=ON -DAZ_USE_PHYSX5:BOOL=ON -DLY_3RDPARTY_PATH="${O3DE_HOME}/3rdParty"
cmake --build build/linux --config profile --target ${PROJECT_NAME} Editor ${PROJECT_NAME}.Assets 
# Clean up
rm -rf /var/lib/apt/lists/*  /tmp/* /var/tmp/*
apt-get clean && apt autoclean