# 启动容器
1. install [docker-desktop](https://docs.docker.com/desktop/install/linux-install/)
```bash
# 在git repository根目录下执行安装脚本
./install.sh
# 如果需要安装nvidia-container-toolkits
./install.sh nvidia
```
2. build docker image and run container
修改docker-compose.yml中的`build:args`字段，指定基础镜像版本如`BASE_IMAGE=nomachine-desktop:22.04`，然后运行
```bash
sudo docker compose up
```

* 基于`nvidia/cuda`基础镜像的TAG：`18.04-cu11.0.3`, `20.04-cu11.0.3`等, 命名规则为`{UBUNTU VERSION}-cu{CUDA VERSION}`, 其中cuda的版本号支持列表见[Docker Image <nvidia/cuda>](https://gitlab.com/nvidia/container-images/cuda/-/blob/master/doc/supported-tags.md)
# ssh连接
```bash
ssh docker@127.0.0.1 -p 10022
```
- `-p 10022`表示指定端口10022

# nomachine连接
选择`NX`协议，`14000`端口，输入用户名和密码，即可连接。
![nomachine连接](nomachine.png)

# 使用VirtualGL硬件加速

nomachine自带了VirtualGL，vglrun路径为`/usr/NX/scripts/vgl/vglrun`，root下需使用全路径，或者加入PATH环境变量。
```bash
vglrun glxinfo | grep -i "opengl"
#显示包含VirtualGL则表示正确 host主机上的DISPLAY必须为:0.
```
运行3D软件时，需要加上vglrun 命令前缀，如
```bash
vglrun gazebo
vglrun ./blender
```

>不使用vglrun也能运行3D程序，将采用软件OpenGL模拟，占用大量CPU。

注意：宿主机需要运行xhost允许所有用户访问X11服务（运行一次即可）
```bash
xhost +
```
否则会出现`[VGL] ERROR: Could not open display :0`错误。