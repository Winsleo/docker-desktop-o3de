ARG BASE_IMAGE=nomachine-desktop:ros-conda22.04-cu11.7.1
FROM $BASE_IMAGE

WORKDIR /opt
ADD scripts/o3de.sh /scripts/
RUN chmod +x /scripts/* 
RUN /scripts/o3de.sh